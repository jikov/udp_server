#ifndef DEBUG_H_
#define DEBUG_H_

#include <stdarg.h>
#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>

#define DBG_ENABLED			1	// General (admin) enable / disable

/*
 * Threads
 */
#define OS_YIELD()						pthread_yield()
#define OS_MUTEX_T						pthread_mutex_t
#define OS_MUTEX_INITIALIZE(p_mutex)	os_mutex_initialize(p_mutex)
#define OS_MUTEX_LOCK(p_mutex)			pthread_mutex_lock(p_mutex)
#define OS_MUTEX_UNLOCK(p_mutex)		pthread_mutex_unlock(p_mutex)

// WARNING: OS_EVENT_XXX() MUST be called only between OS_MUTEX_LOCK and OS_MUTEX_UNLOCK
#define OS_EVENT_T						pthread_cond_t
#define OS_EVENT_INITIALIZE(p_event)	os_event_initialize(p_event)
#define OS_EVENT_SIGNAL(p_event	)		pthread_cond_signal(p_event)
#define OS_EVENT_WAIT(p_event, p_mutex)						pthread_cond_wait(p_event, p_mutex);
#define OS_EVENT_WAIT_TIME(p_event, p_mutex, abstime)		os_event_wait_time(p_event, p_mutex, abstime)
#define OS_EVENT_WAIT_TIMEOUT(p_event, p_mutex, timeout_ms)	os_event_wait_timeout(p_event, p_mutex, timeout_ms)

#define OS_THREAD_T										pthread_t
#define OS_THREAD_FUNCTION(thread_name, arg_name)		static void * thread_name(void *arg_name)
#define OS_THREAD_CREATE(p_thread, thread_name, arg)	pthread_create(p_thread, NULL, thread_name, arg)

#ifdef DBG_ENABLED
#	define DBG(...)						dbg_printf(__VA_ARGS__)
#	define DBGL(...)					dbg_printf("\n" __VA_ARGS__)
#	define DBG_DUMP(buf, len)			dbg_dump(NULL, (buf), (len))
#	define DBG_DUMP_MIN(buf, len)		dbg_dump_min(NULL, (buf), (len))
#	define DBG_DUMPT(title, buf, len)	dbg_dump((title), (buf), (len))
#   define DBG_FLUSH()					dbg_flush();
#else
#	define DBG(...)
#	define DBGL(...)
#	define DBG_DUMP(...)
#	define DBG_DUMP_MIN(...)
#	define DBG_DUMPT(...)
#   define DBG_FLUSH()
#endif

void dbg_init(void);
void dbg_start_thread(void);

void dbg_vprintf(const char *format, va_list vl);
void dbg_printf(const char *format, ...);
void dbg_dump(const char *title, const void *buf, int bytes);
void dbg_dumpa(const char *title, const void *buf, int bytes, unsigned addr);
void dbg_dump_min(const char *title, const void *buf, int bytes);
void dbg_puts(const char *msg);
void dbg_flush(void);

#endif /* DEBUG_H_ */
