#include "debug.h"

#include <stdarg.h>
#include <stdio.h>


#define DBG_ABUF_SIZE		200
#define DBG_BUF_SIZE		0x800
#define DBG_BUF_SIZE_MASK	(DBG_BUF_SIZE - 1)
#define DBG_OVERFLOW_CHAR	'#';

#define INC(idx)	(((idx) + 1) & DBG_BUF_SIZE_MASK)
#define INC_HEAD	INC(DB.idx_head)
#define INC_TAIL	INC(DB.idx_tail)

#define IS_EMPTY(db)	(((db)->idx_head == (db)->idx_tail) && !(db)->full)

static struct dbg_buf {
    OS_MUTEX_T lock;
    OS_EVENT_T event_data;
    OS_EVENT_T event_empty;
    char abuf[DBG_ABUF_SIZE];	// assembly buffer
    char buf[DBG_BUF_SIZE];
    int idx_head;
    int idx_tail;
    int full;
} DB;

//------------------------------------------------------------------------------
void os_event_initialize(OS_EVENT_T *event)
{
    /*
     * Configure condition to use CLOCK_MONOTONIC as a clock source.
     * This allows to use pthread_cond_timedwait() for timeouts.
     * With CLOCK_REALTIME, there are issues when date-time is changed by e.g.
     * NTP server.
     *
     */
    pthread_condattr_t attr;
    int result;

    result = pthread_condattr_init(&attr);
    assert(0 == result);
    result = pthread_condattr_setclock(&attr, CLOCK_MONOTONIC);
    assert(0 == result);
    result = pthread_cond_init(event, &attr);
    assert(0 == result);
}

//------------------------------------------------------------------------------
void os_mutex_initialize(OS_MUTEX_T* mutex)
{
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(mutex, &attr);
}

//------------------------------------------------------------------------------
void dbg_puts(const char *msg)
{
    struct dbg_buf	*db = &DB;
    char *last;

    OS_MUTEX_LOCK(&db->lock);

    for ( ; *msg; msg++)
    {
        if (db->full)
        {
            // Buffer is full - rewrite the last char by overflow char
            last = (db->idx_head) ? &db->buf[db->idx_head - 1] : &db->buf[DBG_BUF_SIZE - 1];
            *last = DBG_OVERFLOW_CHAR;
            break;
        }

        db->buf[db->idx_head] = *msg;
        db->idx_head = INC(db->idx_head);

        if (db->idx_head == db->idx_tail)
            db->full = 1;
    }

    OS_EVENT_SIGNAL(&db->event_data);
    OS_MUTEX_UNLOCK(&db->lock);
}

//------------------------------------------------------------------------------
void dbg_vprintf(const char *format, va_list vl)
{
    OS_MUTEX_LOCK(&DB.lock);

    vsnprintf(DB.abuf, sizeof(DB.abuf), format, vl);
    DB.abuf[sizeof(DB.abuf) - 1] = '\0';	// just to be sure
    dbg_puts(DB.abuf);

    OS_MUTEX_UNLOCK(&DB.lock);
}

//------------------------------------------------------------------------------
void dbg_printf(const char *format, ...)
{
    va_list vl;
    va_start(vl, format);
    dbg_vprintf(format, vl);
    va_end(vl);
}

//------------------------------------------------------------------------------
void dbg_dump_min(const char *title, const void *buf, int bytes)
{
    int i;

    OS_MUTEX_LOCK(&DB.lock);

    if (title)
        dbg_puts(title);

    for (i = 0; i < bytes; i++, buf++)
        dbg_printf("%.2x ", *(const unsigned char *)buf);

    OS_MUTEX_UNLOCK(&DB.lock);
}

//------------------------------------------------------------------------------
void dbg_dump(const char *title, const void *buf, int bytes)
{
    dbg_dumpa(title, buf, bytes, 0);
}

//------------------------------------------------------------------------------
void dbg_dumpa(const char *title, const void *buf, int bytes, unsigned addr)
{
    static char line[] = {"0x00000000 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   !................!"};
    char *p_from = (char *)buf;
    char *p_ascii_line;
    char *p_line = line;
    int line_count = 0;
    char c;

    OS_MUTEX_LOCK(&DB.lock);

    if (title)
        dbg_puts(title);

    while (bytes--)
    {
        if (!line_count)
        {
            p_line = line;
            p_ascii_line = &line[62];
            p_line += sprintf(p_line, "0x%.8X", addr);
        }

        *(p_line++) = ' ';

        c = (*p_from >> 4) & 0xF;
        *(p_line++) = (c < 10) ? '0' + c : 'A' - 10 + c;

        c = *p_from & 0xF;
        *(p_line++) = (c < 10) ? '0' + c : 'A' - 10 + c;

        if (*p_from < ' '  ||  *p_from > '~')
            *(p_ascii_line++) = '.';
        else
            *(p_ascii_line++) = *p_from;

        p_from++;

        if (++line_count == 16)
        {
            dbg_puts("\n");
            dbg_puts(line);
            addr += 16;
            line_count = 0;
        }
    }

    if (line_count > 0)
    {
        do {
            *(p_line++) = ' ';
            *(p_line++) = ' ';
            *(p_line++) = ' ';
            *(p_ascii_line++) = ' ';
        } while (++line_count < 16);

        dbg_puts("\n");
        dbg_puts(line);
    }

    dbg_puts("\n");
    OS_MUTEX_UNLOCK(&DB.lock);
}

//------------------------------------------------------------------------------
OS_THREAD_FUNCTION(dbg_thread, arg)
{
    struct dbg_buf	*db = &DB;

    //dbg_printf("\nDebug thread started");

    while(1)
    {
        // Copy all bytes to modbus slave tx buffer
        OS_MUTEX_LOCK(&db->lock);
        while (!IS_EMPTY(db))
        {
            putchar(db->buf[db->idx_tail]);
            db->idx_tail = INC(db->idx_tail);
            db->full = 0;
        }

        fflush(stdout);
        OS_EVENT_SIGNAL(&db->event_empty);
        OS_EVENT_WAIT(&db->event_data, &db->lock);
        OS_MUTEX_UNLOCK(&db->lock);
    }

    return NULL;
}

//------------------------------------------------------------------------------
void dbg_flush(void)
{
    struct dbg_buf	*db = &DB;

    OS_MUTEX_LOCK(&db->lock);
    OS_EVENT_WAIT(&db->event_empty, &db->lock);
    OS_MUTEX_UNLOCK(&db->lock);
}

//------------------------------------------------------------------------------
void dbg_init(void)
{
    OS_MUTEX_INITIALIZE(&DB.lock);
    OS_EVENT_INITIALIZE(&DB.event_data);
    OS_EVENT_INITIALIZE(&DB.event_empty);
}

//------------------------------------------------------------------------------
void dbg_start_thread(void)
{
    OS_THREAD_T	t;
    OS_THREAD_CREATE(&t, dbg_thread, NULL);
}
