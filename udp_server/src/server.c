
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>

#include "server.h"
#include "files.h"
#include "debug.h"

static struct packetformat packet;
static struct machine_time actual_time;

static int soc_id;
static socklen_t client_len;
static struct sockaddr_in client;

#define PORT 50505

struct ip_addr_struct {
    uint8_t addr0;
    uint8_t addr1;
    uint8_t addr2;
    uint8_t addr3;
};

//------------------------------------------------------------------------
static void close_server_handler()
{
    DBGL("\nClosing server ...");
    close(soc_id);
    exit(EXIT_SUCCESS);
}

//------------------------------------------------------------------------
int server_init(void)
{
    struct sockaddr_in server;
    struct ip_addr_struct client_addr;

    client_addr.addr0 = 10;
    client_addr.addr1 = 2;
    client_addr.addr2 = 168;
    client_addr.addr3 = 192;

    uint32_t *client_addr_complete = (uint32_t *)&client_addr;

    /* server */
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(PORT);

    /* client */
    memset(&client, 0, sizeof(client));
    client.sin_family = AF_INET;
    client.sin_addr.s_addr = htonl(*client_addr_complete);
    client.sin_port = htons(PORT);

    soc_id = socket(AF_INET, SOCK_DGRAM, 0);
    bind(soc_id, (struct sockaddr *)&server, sizeof(server));

    signal(SIGINT, close_server_handler);

    client_len = sizeof(client_addr);

    return 0;
}

//------------------------------------------------------------------------
static int is_internet_connection(void)
{
    int socket_desc, is_internet = -1;
    char *hostname = "www.google.com";
    char ip[25];
    struct in_addr *addrlist;
    struct addrinfo address_info, *address_info_result, *rp;
    static int idx; /* index of an address in addrlist */

    memset(&address_info, 0, sizeof(struct addrinfo));
    address_info.ai_family = AF_INET;    /* Allow IPv4 or IPv6 */
    address_info.ai_socktype = SOCK_STREAM;
    address_info.ai_flags = AI_PASSIVE;    /* For wildcard IP address */
    address_info.ai_protocol = 0;          /* Any protocol */
    address_info.ai_canonname = NULL;
    address_info.ai_addr = NULL;
    address_info.ai_next = NULL;

    is_internet = getaddrinfo(hostname, "http", &address_info, &address_info_result);

    if (is_internet != 0)
    {
        is_internet = -1;
        return is_internet;
    }

    /* getaddrinfo should inform about Internet availability,
     * but in order to be precise, create socket and connection */

    /* getaddrinfo() returns a list of address structures.
      Try each address until we successfully connect(2).
      If socket(2) (or connect(2)) fails, we (close the socket and) try the next address. */
    for (rp = address_info_result; rp != NULL; rp = rp->ai_next)
    {
        socket_desc = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);

        if (socket_desc == -1)
        {
            idx++;
            continue;
        }

        addrlist = (struct in_addr *) rp->ai_addr;
        strcpy(ip, inet_ntoa(addrlist[idx]));
        idx++;

        if (connect(socket_desc, rp->ai_addr, rp->ai_addrlen) != -1)
        {
            DBG("Connected to: ");
            DBG(ip);
            DBGL();
            is_internet = 0;
            break; /* break on success */
        }

        close(socket_desc);
        is_internet = -1;
    }

    close(socket_desc);
    freeaddrinfo(address_info_result);
    DBGL("Connection closed.");
    idx = 0;

    return is_internet;
}

#define YEAR_OFFSET             100 /* year sice 1900 */
#define SECONDS_DIFFERENCE      50
//------------------------------------------------------------------------
static int time_compare(struct machine_time *actual_time)
{
    int result = -1;
    struct tm station_time, *server_time;
    time_t now;
    double diff_seconds;
    struct packetformat *datapacket = &packet;

    time(&now);
    server_time = localtime(&now);
    DBGL("%02d:%02d", server_time->tm_hour, server_time->tm_min);

    actual_time->year = server_time->tm_year - YEAR_OFFSET;
    actual_time->month = server_time->tm_mon + 1;
    actual_time->day = server_time->tm_mday;
    actual_time->hour = server_time->tm_hour;
    actual_time->min = server_time->tm_min;
    actual_time->sec = server_time->tm_sec;

    /* year value from station must be only 16 in case of year 2016 */
    station_time.tm_year = datapacket->year + YEAR_OFFSET;
    station_time.tm_mon = datapacket->month - 1;
    station_time.tm_mday = datapacket->day;
    station_time.tm_hour = datapacket->hour;
    station_time.tm_min = datapacket->min;
    station_time.tm_sec = 0;

    diff_seconds = difftime(now, mktime(&station_time));

    if ((diff_seconds > SECONDS_DIFFERENCE) || (diff_seconds < -SECONDS_DIFFERENCE))
    {
        result = -1;
        /* yes, we need to set time at the station */
        actual_time->timeset = 'Y';
    }
    else
    {
        result = 0;
        /* no, we neednt to set time at the station */
        actual_time->timeset = 'N';
    }

    return result;
}

/*******************************************************************************
  * @function	Get_Value_Sign
  * @brief  	Get sign of given value
  * @param  	value: value to be evaluated
  * @retval 	sign: 1 - value is positive
  *                  -1 - value is negative
  *****************************************************************************/
static int16_t get_value_sign(int16_t value)
{
    int16_t sign;

    if (value >= 0)
    {
        sign = 1;
    }
    else
    {
        sign = -1;
    }

    return sign;
}

/*******************************************************************************
  * @function	Get_Printing_Sign
  * @brief  	Get sign for printing
  * @param  	sign: value to be evaluated
  * @retval 	'-' - 'minus' in case of negative number
  *             ' ' - 'space' in case of positive number
  *****************************************************************************/
static char get_printing_sign(int16_t sign)
{
    char c;

    if (sign < 0)
        c = '-';
    else
        c = ' ';

    return c;
}

//---------------------------------------------------------------------------
static int save_data(struct packetformat *datapacket)
{
    FILE *pfile;
    char file_buf[13]; /* string for a file name (name format: YEARMONTHDAY.txt) */
    char dir_buf[5]; /* string for a dir name (name format: 2016) */
    char path[18];
    int val;
    int16_t coeff_sign_actual_temp, coeff_sign_gradient_temp, coeff_sign_dew_point, coeff_sign_MPL115_temp, coeff_sign_SHT21_temp;
    char sign_actual_temp, sign_gradient_temp, sign_dew_point, sign_MPL115_temp, sign_SHT21_temp;

    memset(path, 0, sizeof(path));

    file_buf[0] = '2';
    file_buf[1] = '0'; /* because year will "always" start with 20xx (e.g. 2015) */

    file_buf[2] = (datapacket->year - 2000)/10 + '0';
    file_buf[3] = (datapacket->year - 2000)%10 + '0';
    file_buf[4] = datapacket->month/10 + '0';
    file_buf[5] = datapacket->month%10 + '0';
    file_buf[6] = datapacket->day/10 + '0';
    file_buf[7] = datapacket->day%10 + '0';
    file_buf[8] = '.';
    file_buf[9] = 't';
    file_buf[10] = 'x';
    file_buf[11] = 't';
    file_buf[12] = '\0';

    dir_buf[0] = file_buf[0];
    dir_buf[1] = file_buf[1];
    dir_buf[2] = file_buf[2];
    dir_buf[3] = file_buf[3];
    dir_buf[4] = '\0';

    if (!fDirExists(dir_buf)) /* dir does not exist -> create dir */
    {
       val = fDirCreate(dir_buf, 0);
       if (val != 0)
       {
            return -1;
       }
    }

    strcat(path, dir_buf);
    strcat(path, "/");
    strcat(path, file_buf);

    pfile = fopen(path, "a+");

    if (pfile == NULL)
        return -1;

    /* pretend to writing sign before the decimal part */
    coeff_sign_actual_temp = get_value_sign(datapacket->DS18B20_temperature);
    coeff_sign_gradient_temp = get_value_sign(datapacket->temperature_gradient);
    coeff_sign_MPL115_temp = get_value_sign(datapacket->MPL115_temperature);
    coeff_sign_SHT21_temp = get_value_sign(datapacket->SHT21_temperature);
    coeff_sign_dew_point = get_value_sign(datapacket->dew_point);

    val = fprintf(pfile, "%02d.%02d.%4d; %02d:%02d:%02d; %c%02d.%d; %c%02d.%d; %c%02d.%d; %c%02d.%d; %03d.%d; %03d.%d; %02d.%d; %c%02d.%d\n",\
            datapacket->day, datapacket->month, datapacket->year, \
            datapacket->hour, datapacket->min, datapacket->sec, \

            sign_actual_temp = get_printing_sign(coeff_sign_actual_temp), \
            datapacket->DS18B20_temperature/10 * coeff_sign_actual_temp, \
            datapacket->DS18B20_temperature%10 * coeff_sign_actual_temp, \

            sign_gradient_temp = get_printing_sign(coeff_sign_gradient_temp), \
            datapacket->temperature_gradient/10 * coeff_sign_gradient_temp, \
            datapacket->temperature_gradient%10 * coeff_sign_gradient_temp, \

            sign_MPL115_temp = get_printing_sign(coeff_sign_MPL115_temp), \
            datapacket->MPL115_temperature/10 * coeff_sign_MPL115_temp, \
            datapacket->MPL115_temperature%10 * coeff_sign_MPL115_temp, \

            sign_SHT21_temp = get_printing_sign(coeff_sign_SHT21_temp), \
            datapacket->SHT21_temperature/10 * coeff_sign_SHT21_temp, \
            datapacket->SHT21_temperature%10 * coeff_sign_SHT21_temp, \

            datapacket->MPL115_pressure_actual/10,\
            datapacket->MPL115_pressure_actual%10, \

            datapacket->MPL115_pressure_avg/10, \
            datapacket->MPL115_pressure_avg%10, \

            datapacket->SHT21_humidity/10, \
            datapacket->SHT21_humidity%10, \

            sign_dew_point = get_printing_sign(coeff_sign_dew_point), \
            datapacket->dew_point/10 * coeff_sign_dew_point, \
            datapacket->dew_point%10*coeff_sign_dew_point);

    fclose (pfile);

    if (val <= 0)
        return -1;

    return 0;
}

//------------------------------------------------------------------------
static int git_add_changes(void)
{
    int ret;

    ret = system("git add .");

    return ret;
}

//------------------------------------------------------------------------
static int git_add_commit(void)
{
    int ret;
    struct tm *time_now;
    time_t now;
    char buf_cmd[24];
    char buf_time[8];
    char *commit = "git commit -m ";
    char quote = '"';

    time(&now);
    time_now = localtime(&now);

    /* year since 1900 */
    buf_time[0] = (time_now->tm_year+1900)/1000 + '0';
    buf_time[1] = (time_now->tm_year+1900)%1000/100 + '0';
    buf_time[2] = (time_now->tm_year+1900)%1000%100/10 + '0';
    buf_time[3] = (time_now->tm_year+1900)%1000%100%10 + '0';
    buf_time[4] = time_now->tm_mon/10 + '0';
    buf_time[5] = time_now->tm_mon%10 + 1 + '0'; /* month is 0-11 */
    buf_time[6] = time_now->tm_mday/10 + '0';
    buf_time[7] = time_now->tm_mday%10 + '0';
    //TODO: pridat i cas ?

    strcat(buf_cmd, commit);
    strncat(buf_cmd, &quote, 1);
    strcat(buf_cmd, buf_time);
    strncat(buf_cmd, &quote, 1);

    ret = system(buf_cmd);

    return ret;
}

//------------------------------------------------------------------------
static int git_push(void)
{
    int ret;

    ret = system("git push origin master");

    return ret;
}

//------------------------------------------------------------------------
void server_periodic(void)
{
    int result, net_result;

    DBGL("Waiting for packet from client ...");
    result = recvfrom(soc_id, &packet, sizeof(struct packetformat), 0, (struct sockaddr *)&client, &client_len);

    if (result < 0)
    {
        perror("recvfrom sd");
        exit(EXIT_FAILURE);
    }
    else
    {
        DBG("Incoming data: ");
        DBG("year: %d, month: %d, day: %d, ", packet.year, packet.month, packet.day);
        DBG("%d:%d\r\n", packet.hour, packet.min);
        DBG("DS18B20 temp: %d.%d\r\n", packet.DS18B20_temperature/10, packet.DS18B20_temperature%10);
        DBG("temp gradient: %d.%d\r\n", packet.temperature_gradient/10, packet.temperature_gradient%10);
        DBG("MPL115 temp: %d.%d\r\n", packet.MPL115_temperature/10, packet.MPL115_temperature%10);
        DBG("SHT21 temp: %d.%d\r\n", packet.SHT21_temperature/10, packet.SHT21_temperature%10);
        DBG("pressure actual: %d.%d\r\n", packet.MPL115_pressure_actual/10, packet.MPL115_pressure_actual%10);
        DBG("pressure avg: %d.%d\r\n", packet.MPL115_pressure_avg/10, packet.MPL115_pressure_avg%10);
        DBG("humidity: %d.%d\r\n", packet.SHT21_humidity/10, packet.SHT21_humidity%10);
        DBG("dew point: %d.%d\r\n", packet.dew_point/10, packet.dew_point%10);
        DBG("station status: %X\r\n", packet.station_status);

        result = save_data(&packet);

        if (result != 0)
        {
             actual_time.server_status = SERVER_FILE_ERROR;
        }

        net_result = is_internet_connection();
        if (net_result == 0)
        {
            result = time_compare(&actual_time);
            /* if station time and server time are different than limit */
            if (result != 0)
            {
                DBGL("Write time to client.");
            }
            else
            {
                DBGL("Time of the station and server is almost the same.");
            }

        }
        else
        {
            actual_time.timeset = 'N';
            DBGL("No internet connection.");
        }
        /* send response + time to station */
        sendto(soc_id, &actual_time, sizeof(actual_time), MSG_DONTWAIT, (struct sockaddr *)&client, client_len);

        result = git_add_changes();
        result = git_add_commit();
 //TODO: pridat vyhodnoceni navratove hodnoty a poslat zpatky server_status
        if(net_result == 0) /* call it only if internet connection is available */
        {
            result = git_push(); //TODO: volat kazde 2 hodiny

        }
    }
}
