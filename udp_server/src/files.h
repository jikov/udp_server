#ifndef FILES_H_
#define FILES_H_

#include <stdio.h>

#define concat_on_stack(s1, s2)			strcat(strcpy(alloca(strlen(s1) + strlen(s2) + 1), (s1)), (s2))
#define concat_filename(path, fname)	(!path) ? (fname) : concat_on_stack(path, fname)

int 	fDirExists(const char *p_dirname);
int 	fDirEmpty(const char *p_dirname);
int 	fDirCreate(const char *p_dirname, mode_t mode);
int 	fDirTreeCreate(const char *p_dirtree, mode_t mode);
int 	fFileExists(const char *p_filename);
int 	fFileCopy(const char *p_src_filename, const char *p_dst_filename);
int 	fFileIO(const char *p_filename, const char *modes, int (* callback)(FILE *f));

int		fReadBool(const char *p_filename);
int		fReadString(const char *p_filename, char *p_buf, int max_len);
char*	fReadLine(const const char *p_filename);
int		fReadLines(const char *p_filename, int callback(char *p_line, unsigned lineno));
void	fWriteBool(const char *p_filename, int value);
int		fWriteString(const char *p_filename, const char *p_format, ...);
int		fWriteVstring(const char *p_filename, const char *p_format, va_list args);
int		fAppendString(const char *p_filename, char *p_format, ...);
int		fAppendVstring(const char *p_filename, char *p_format, va_list args);

#endif /* FILES_H_ */
