#include "server.h"
#include "debug.h"

int main(void)
{
    /* init and start debug prints */
    dbg_init();
    dbg_start_thread();

    /* init server and client addresses */
    server_init();

    while (1)
    {
        server_periodic();
        //system("git push origin master");
        //printf("Done.\n");
        //}
    }

    return 0;
}
