#ifndef GET_TIME_H
#define GET_TIME_H

#include <time.h>
#include <unistd.h>
#include <stdint.h>

typedef enum error_mask {
    NO_ERROR            = 0x00,
    SDCARD_ERROR_MASK   = 0x01,
    CAN_ERROR_MASK      = 0x02,
    ETH_ERROR_MASK      = 0x04,
    TIME_ERROR_MASK     = 0x08,
    SENSOR_ERROR_MASK   = 0x10,
} station_errors_t;

enum server_sts {
    SERVER_NO_ERROR,
    SERVER_FILE_ERROR,
};

struct packetformat{
    uint16_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t min;
    uint8_t sec;
    int16_t DS18B20_temperature;
    int16_t temperature_gradient;
    int16_t MPL115_temperature;
    int16_t SHT21_temperature;
    uint16_t MPL115_pressure_actual;
    uint16_t MPL115_pressure_avg;
    uint16_t SHT21_humidity;
    int16_t dew_point;
    station_errors_t station_status;
};

struct machine_time {
    uint8_t server_status;
    uint8_t timeset; /* 'Y' for yes / 'N' for no time settings is needed */
    uint16_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t min;
    uint8_t sec;
};

int server_init(void);
void server_periodic(void);

#endif /* GET_TIME_H */
