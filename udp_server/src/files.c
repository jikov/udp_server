#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <unistd.h>

//#include "../misc/debug.h"
//#include "../misc/errors.h"
//------------------------------------------------------------------------------
int fDirExists(const char *p_dirname)
{
    struct stat st;

    // get size and permissions of the source file
    if (stat(p_dirname, &st) || !S_ISDIR(st.st_mode))
        return 0;

    return 1;
}

//------------------------------------------------------------------------------
int fDirEmpty(const char *p_dirname)
{
    int n = 0;
    struct dirent *d;
    DIR *dir;

    if (!(dir = opendir(p_dirname)))
        return 1;	// Not a directory or doesn't exist

    while ((d = readdir(dir)) != NULL)
        if(++n > 2)
            break;

    closedir(dir);

    return (n <= 2);
}

//------------------------------------------------------------------------------
int fDirCreate(const char *p_dirname, mode_t mode)
{
    struct stat st;

    // set default mode if mode is zero
    if (!mode)
        mode = S_IRWXU | S_IRWXG | S_IRWXO;

    if (stat(p_dirname, &st) != 0)
    {
        if (mkdir(p_dirname, mode) != 0 && errno != EEXIST)
            return -1;
    }
    else if (!S_ISDIR(st.st_mode))
    {
        errno = ENOTDIR;
        return -1;
    }

    return 0;
}

//------------------------------------------------------------------------------
int fDirTreeCreate(const char *p_dirtree, mode_t mode)
{
    char *p_pos;
    char *p_sep;
    char *p_tree;
    int result = -1;

    if (!(p_tree = strdup(p_dirtree)))
        return -1;

    for (p_pos = p_tree; (p_sep = strchr(p_pos, '/')); p_pos = p_sep + 1)
    {
        // skip root and double slash
        if (p_sep == p_pos)
            continue;

        *p_sep = '\0';

        if (fDirCreate(p_tree, mode))
            goto err;

        *p_sep = '/';
    }

    if (fDirCreate(p_dirtree, mode))
        goto err;

    result = 0;

err:
    free(p_tree);
    return result;
}

//------------------------------------------------------------------------------
int fFileExists(const char *p_filename)
{
    struct stat st;

    // get size and permissions of the source file
    if (stat(p_filename, &st) || !(S_ISREG(st.st_mode) || S_ISBLK(st.st_mode) || S_ISCHR(st.st_mode)) )
        return 0;

    return 1;
}

//------------------------------------------------------------------------------
int fFileCopy(const char *p_src_filename, const char *p_dst_filename)
{
    int src = -1;
    int dst = -1;
    struct stat stat_buf;
    int result = -1;

    // open source file
    if ((src = open(p_src_filename, O_RDONLY)) == -1)
        goto err;

    // get size and permissions of the source file
    if (fstat(src, &stat_buf))
        goto err;

    // open destination file
    if ((dst = open(p_dst_filename, O_WRONLY | O_CREAT, stat_buf.st_mode)) == -1)
        goto err;

    // copy file using sendfile (the fastest copy inside kernel space)
    if (sendfile (dst, src, NULL, stat_buf.st_size) != stat_buf.st_size)
        goto err;

    result = 0;

err:
    if (dst != -1)
        close(dst);

    if (src != -1)
        close(src);

    return result;
}

//------------------------------------------------------------------------------
int fFileIO(const char *p_filename, const char *modes, int (* callback)(FILE *f))
{
    FILE    *f;
    int     result;

    if (!(f = fopen(p_filename, modes)))
    {
        //DBG_PERROR("Cannot open file \"%s\"", p_filename);
        return -1;
    }

    result = callback(f);

    if (ferror(f))
       // DBG_PERROR("File I/O error \"%s\"", p_filename);

    fclose(f);

    return result;
}

//------------------------------------------------------------------------------
int fReadBool(const char *p_filename)
{
    int read_bool(FILE *f)
    {
        return (fgetc(f) == '1');
    }

    return fFileIO(p_filename, "r", read_bool);
}

//------------------------------------------------------------------------------
void fWriteBool(const char *p_filename, int value)
{
    int write_bool(FILE *f)
    {
        return fputc(value ? '1' : '0', f) < 0 ? -1 : 0;
    }

    fFileIO(p_filename, "w", write_bool);
}

//------------------------------------------------------------------------------
int fReadLines(const const char *p_filename, int callback(char *p_line, unsigned lineno))
{
    int read_lines(FILE *f)
    {
        int result = 0;
        char *p_line = NULL;
        size_t len = 0;
        unsigned lineno = 0;
        ssize_t n;

        while(1)
        {
            // read line (line buffer is allocated automatically)
            if (((n = getline(&p_line, &len, f)) < 0))
            {
                if (feof(f))
                    break;

                result = -1;
                break;  // error
            }

            ++lineno;

            // replace terminating CRLF by \0
            *strpbrk(p_line, "\r\n\0") = '\0';

            // process line
            if (callback)
                if ((result = callback(p_line, lineno)) != 0)
                    break;
        }

        if (p_line)
            free(p_line);

        return result;
    }

    return fFileIO(p_filename, "r", read_lines);
}

//------------------------------------------------------------------------------
char * fReadLine(const char *p_filename)
{
    static char *p_line = NULL;
    static unsigned len = 0;

    int read_first_line(FILE *f)
    {
        if (getline(&p_line, &len, f) < 0)
            return -1;

        return 0;
    }

    return fFileIO(p_filename, "r", read_first_line) ? NULL : p_line;
}

//------------------------------------------------------------------------------
int fReadString(const char *p_filename, char *p_buf, int max_len)
{
    int len;
    p_buf[0] = '\0';

    int read_string(FILE *f)
    {
        fgets(p_buf, max_len, f);

        // remove "new line" character
        len = strlen(p_buf);
        if (p_buf[len-1] == '\n')
            p_buf[len-1] = '\0';

        return 0;
    }

    return fFileIO(p_filename, "r", read_string);
}

//------------------------------------------------------------------------------
int fWriteVstring(const char *p_filename, const char *p_format, va_list args)
{
    int write_string(FILE *f)
    {
        return vfprintf(f, p_format, args)  < 0 ? -1 : 0;
    }

    return fFileIO(p_filename, "w", write_string);
}

//------------------------------------------------------------------------------
int fWriteString(const char *p_filename, const char *p_format, ...)
{
    int result;
    //va_list args;
    //va_start(args, p_format);
   // result = fWriteVstring(p_filename, p_format, args);
    //va_end(args);
    return result;
}

//------------------------------------------------------------------------------
int fAppendVstring(const char *p_filename, char *p_format, va_list args)
{
    int write_string(FILE *f)
    {
        return vfprintf(f, p_format, args)  < 0 ? -1 : 0;
    }

    return fFileIO(p_filename, "w+", write_string);
}

//------------------------------------------------------------------------------
int fAppendString(const char *p_filename, char *p_format, ...)
{
    int result;
   // va_list args;
    //va_start(args, p_format);
   // result = fAppendVstring(p_filename, p_format, args);
   // va_end(args);
    return result;
}
